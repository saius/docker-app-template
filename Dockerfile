FROM docker-app-base

# agregamos listas non-free cuando se requieran este tipo de dependencias
#RUN echo 'deb http://ftp.de.debian.org/debian buster main non-free' >> /etc/apt/sources.list
#RUN apt-get update

# para buscar un paquete y salir
#RUN apt search APP && exit 1

# instalar paquete acá
RUN apt install -y APP

## si quisieramos intalar desde un .deb
# copiar archivos necesarios (previamente descargados)
#COPY ARCHIVO.deb /ARCHIVO.deb
# instalar paquetes
#RUN apt-get install -yf /ARCHIVO.deb
