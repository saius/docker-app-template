Este template es para crear apps usando como imagen base
[saius/docker-app-base](https://gitlab.com/saius/docker-app-base)

Para testear una app cambiar el `Dockerfile` y build-ear y correr localmente:

```
docker build -t docker-app-template .
x11docker --sudouser --desktop --size 800x600 docker-app-template
```

Una vez alcanzado lo deseado copiar el proyecto y cambiarle el nombre
para dejarlo como una app fija y continuar desde ahí

Para tener la home persistente en el container agregar al comando de `x11docker` la opción

```
--home=`realpath ./home`
```

y ahora estará sincronizada con la carpeta `home` de este proyecto

---

## Documentación base
Primero instalamos x11docker localmente con el administrador de paquetes de tu distro 
o como dice en https://github.com/mviereck/x11docker#installation

Vamos a usar esta imagen de XFCE https://hub.docker.com/r/x11docker/xfce
(https://github.com/mviereck/dockerfile-x11docker-xfce)

Para probarla rápido localmente:

`x11docker --desktop --size 320x240 x11docker/lxde`

Para crear la imagen de este proyecto:

`docker build -t NOMBBRE_IMAGEN .`

Y la corremos con x11docker:

`x11docker --desktop --size 800x600 NOMBRE_IMAGEN`

O con sudo

`x11docker --sudouser --desktop --size 800x600 NOMBRE_IMAGEN`

(acá usamos la resolución 800x600 pero puede ser 1024x768 o cualquiera que quieran)

Además podemos ver el container andando:

`docker ps`

O conectarnos a su terminal:

`docker exec -it NOMBRE_CONTAINER bash`

Si queremos logearnos como root dentro del container primero hay que pasarle 
`--sudouser` al comando x11docker. Después ejecutar `su` dentro del container,
 contraseña "x11docker".

Tener en cuenta que cualquier modificación hecha al container una vez 
abierto va a ser descartada cuando cierren la ventana del XFCE o corten 
la ejecución del comando x11docker.

Para más imágenes de desktops environments que no sean XFCE visitar 
https://hub.docker.com/u/x11docker/
